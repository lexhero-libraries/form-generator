# Form Generator

> **Form Generator** è una libreria che permette di creare, gestire e validare form in maniera dinamica e modulare. L’obiettivo principale è fornire un’interfaccia semplice per generare form HTML (o di altro tipo) in modo efficace, con poche righe di codice e definizioni dichiarative.

---

## Indice

- [Caratteristiche](#caratteristiche)
- [Installazione](#installazione)
- [Prerequisiti](#prerequisiti)
- [Come iniziare](#come-iniziare)
- [Struttura del Progetto](#struttura-del-progetto)
- [Utilizzo](#utilizzo)
  - [Esempio di utilizzo di base](#esempio-di-utilizzo-di-base)
- [API](#api)
  - [Opzioni di Configurazione](#opzioni-di-configurazione)
- [Validazione](#validazione)
- [Esempi Completi](#esempi-completi)

---

## Caratteristiche

- **Generazione dinamica dei form**: definisci campi, tipi di input, etichette e validazioni in un singolo oggetto di configurazione.
- **Validazione personalizzabile**: supporto a regole di validazione integrate, più la possibilità di crearne di personalizzate.
- **Eventi**: ascolto di eventi come `onSubmit`, `onError`, `onChange`, per reagire in modo dinamico alle azioni dell’utente.

---

## Installazione

È possibile installare la libreria in diversi modi. Se stai usando un gestore di pacchetti (ad esempio npm o yarn), esegui:

```bash
# Usando npm
npm install form-generator --save

# Usando yarn
yarn add form-generator
```

---

## Prerequisiti

- **Node.js** (versione >= 14) se utilizzi il pacchetto npm.
- **Browser moderni** (per un uso lato front-end).  
  - Per browser datati potrebbe essere necessario un *polyfill* (ad esempio [Babel](https://babeljs.io/) o [core-js](https://github.com/zloirock/core-js)).
- **Framework JS**: React

---

## Come iniziare
1. **Clona il repository** o installa la libreria via npm/yarn.
2. **Importa Form Generator** nel tuo progetto (come modulo ES o CommonJS).
3. **Definisci la configurazione** del form: decidi i campi, le validazioni e i comportamenti desiderati.
4. **Inizializza** Form Generator con la configurazione scelta.
5. **Integra** il form generato nella tua applicazione e gestisci la logica di validazione o invio dati.

---

## Utilizzo
### Esempio di utilizzo di base
Supponiamo di avere un file MyForm.tsx in cui vogliamo generare un form con due campi: **nome** e **password**.
```js
import FormGenerator from 'form-generator';
// Configurazione di esempio
const formElements:FormElements = [
    {
        Header:"Nome",
        accessor:"nome",
        type:"text"
    },
    {
        Header:"Password",
        accessor:"password",
        type:"password"
    }
]
const initialValues =()=> {
    return {
        testo:Math.random(),
    }
}
const validationSchema = Yup.object().shape({
    textField: Yup.string().required('Text field').min(5,"troppo corto"),
})

//Il descrittore contiene tutte le informazioni riguardanti la forma del form, 
// i suoi valori iniziali e le validazioni da applicare 
const formDescriptor = new FormDescriptor({elements:formElements,initialValues, validationSchema})

export default function MyForm(){
    return <FormGeneratorContextProvider validateOnChange onChange={(value)=>{console.log("value",value)}} formDescriptor={formDescriptor} onSubmit={()=>{}}>
        <FormGeneratorContext.Consumer>
            {()=>{
                return <div>
                    <section className={"my-3"}>
                        <h3>Nome</h3>
                        <FormElement accessor="nome"/>
                        <h3>Password</h3>
                        <FormElement accessor="password"/>
                    </section>

                    <button type="submit">SAVE</button>
                </div>
            }}
        </FormGeneratorContext.Consumer>
    </FormGeneratorContextProvider>
}


```

### Opzioni di Configurazione
-	TBD


## Validazione
Il motore di validazione della libreria può accettare tutto quello che accetta la libreria Yup.


## Esempi Completi
Nella cartella app del repository troverai progetti dimostrativi con diverse configurazioni, validazioni e stili:

---
