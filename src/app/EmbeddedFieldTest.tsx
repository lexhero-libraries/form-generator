import * as Yup from "yup";
import {FormElements} from "../form-generator/ElementInterface";
import FormDescriptor from "../form-generator/form-descriptor/FormDescriptor";
import FormGeneratorContextProvider from "../form-generator/form-context/FormGeneratorContextProvider";
import FormElement from "../form-generator/form-elements/FormElement";
import {FormGeneratorContext} from "../esm";
import {dictionaryFormElements, dictionaryInitialElements} from "./DictionaryFieldTest";
import useSimulateQuery from "./useSimulateQuery";
import {useEffect} from "react";


const formElements:FormElements = [
    {
        Header:"cazzo",
        accessor:"cazzo",
        type:"text"
    },
    {
        Header:"bomba",
        accessor:"bomba",
        type:"text"
    }
]

const initialvalues = {cazzo:undefined, bomba:"b"}

const companyFormElements:FormElements = [
    {
        accessor:"companyName",
        type:"embedded",
        selectable:true,
        Header:"Company name",
        formElements: formElements,
        initialValues: initialvalues
    }
]
const initialValues = {
}
const validationSchema = Yup.object().shape({
    companyName: Yup.string().required('Company name is required'),
})

const formDescriptor = new FormDescriptor({elements:companyFormElements,initialValues, validationSchema})

export default function EmbeddedFieldTest(){


    return <>
        <FormGeneratorContextProvider formDescriptor={formDescriptor}>
            <FormGeneratorContext.Consumer>
                {()=>{
                    return <TextElement/>
                }}
            </FormGeneratorContext.Consumer>
        </FormGeneratorContextProvider>
    </>
}

const TextElement= () => {
    return <>
        <FormElement accessor="companyName"/>
    </>
}
