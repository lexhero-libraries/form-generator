import * as Yup from "yup";
import {FormElements} from "../form-generator/ElementInterface";
import FormDescriptor from "../form-generator/form-descriptor/FormDescriptor";
import FormGeneratorContextProvider from "../form-generator/form-context/FormGeneratorContextProvider";
import FormElement from "../form-generator/form-elements/FormElement";
import {FormGeneratorContext} from "../esm";


const companyFormElements:FormElements = [
    {
        accessor:"textField",
        type:"wysiwyg",
        Header:"Text field",
        toolbar:false
    }
]
const initialValues =()=> {
    return {
        textField:'<p><br></p>',
    }
}
const validationSchema = Yup.object().shape({
    textField: Yup.string().required('Text field').min(5,"troppo corto"),
})

function handleOnChange(e){
    if (e.value === '<p>ciao</p>') {
        e.reset();
    } else {
        e.defaultSetter()
    }
    
}

const formDescriptor = new FormDescriptor({elements:companyFormElements,initialValues, validationSchema, resetOnSubmit:true})

export default function WYSIWYGFieldTest(){
    return <>
        <FormGeneratorContextProvider validateOnChange onChange={(value)=>{console.log("value",value)}} formDescriptor={formDescriptor} onSubmit={()=>{}}>
            <FormGeneratorContext.Consumer>
                {()=>{
                    return <div>
                        <section className={"my-3"}>
                            <h3>Text element standard</h3>
                            <FormElement accessor="textField" className="pippo" style={{height:200}}  onChange={handleOnChange} />
                        </section>

                        <button type="submit">SAVE</button>
                    </div>
                }}
            </FormGeneratorContext.Consumer>
        </FormGeneratorContextProvider>
    </>
}
