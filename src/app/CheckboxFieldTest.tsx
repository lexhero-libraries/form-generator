import React from "react";
import * as Yup from "yup";
import {FormElements} from "../form-generator/ElementInterface";
import FormDescriptor from "../form-generator/form-descriptor/FormDescriptor";
import FormGeneratorContextProvider from "../form-generator/form-context/FormGeneratorContextProvider";
import FormElement from "../form-generator/form-elements/FormElement";
import {FormGeneratorContext} from "../esm";
import { MyOnChange } from "../form-generator/BasicFormElementInterface";


const companyFormElements:FormElements = [
    {
        accessor:"checkboxA",
        type:"checkbox",
        Header:"Checkbox"
    }
]
const initialValues =()=> {
    return {
        textField:Math.random(),
    }
}
const validationSchema = Yup.object().shape({
    
})

function handleOnChange(e:MyOnChange){
    console.log(e.value, e.accessorCurrentValue);
    
    e.defaultSetter()    
}

const formDescriptor = new FormDescriptor({elements:companyFormElements,initialValues, validationSchema})

export default function CheckboxFieldTest(){
    return <>
        <FormGeneratorContextProvider validateOnChange onChange={(value)=>{}} formDescriptor={formDescriptor} onSubmit={()=>{}}>
            <FormGeneratorContext.Consumer>
                {()=>{
                    return <div>
                        <section className={"my-3"}>
                            <h3>Checkbox</h3>
                            <FormElement accessor="checkboxA" onChange={handleOnChange} />
                        </section>

                        <button type="submit">SAVE</button>
                    </div>
                }}
            </FormGeneratorContext.Consumer>
        </FormGeneratorContextProvider>
    </>
}
