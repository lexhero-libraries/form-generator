import * as Yup from "yup";
import { FormElements } from "../form-generator/ElementInterface";
import FormDescriptor from "../form-generator/form-descriptor/FormDescriptor";
import FormGeneratorContextProvider from "../form-generator/form-context/FormGeneratorContextProvider";
import FormElement from "../form-generator/form-elements/FormElement";
import { FormGeneratorContext } from "../esm";
import { CSSProperties } from "react";
import { IStyleCustomization } from "react-dropzone-uploader";

const dropzoneStyles: IStyleCustomization<CSSProperties> = {
    dropzone: {
        border: "2px dashed #d52f89",
        padding: "20px",
        textAlign: "center",
        fontFamily: "Arial, sans-serif",
        fontSize: "18px",
        borderRadius: "10px",
        backgroundColor: "#fbebf4",
        transition: "background-color 0.2s ease-in-out",
        overflow: 'hidden',
    },
    dropzoneActive: {
        backgroundColor: "rgb(252, 202, 230)",
    }
}

const companyFormElements: FormElements = [
    {
        accessor: "fileL",
        type: "fileList",
        Header: "Company file",
        accept: "*",
        maxFiles: 5,
        maxFileSizeMB: 150,
        showList: true,
        showDropzone: true,
        dropzoneStyles: dropzoneStyles,
        texts: {
            remove: 'Rimuovi',
            upload: '+',
        },
        removeButton: <button type='button' style={{ border: '2px solid black' }}>-</button>,
        dropzoneContent: <span style={{ color: '#d52f89' }}>+</span>
    }
]
const initialValues = {
    companyName: "TOTUCCIO",
}
const validationSchema = Yup.object().shape({
    companyName: Yup.string().required('Company name is required'),
})

const formDescriptor = new FormDescriptor({ elements: companyFormElements, initialValues, validationSchema })

function handleRemoveFile(fileList, index, setFieldValue) {
    const newFiles = fileList.filter((_: any, i: number) => i !== index);
    setFieldValue("fileL", newFiles);
}

export default function FileListFieldTest() {
    return <>
        <FormGeneratorContextProvider formDescriptor={formDescriptor}>
            <FormGeneratorContext.Consumer>
                {(context) => {
                    const fileList = context.getFieldValue("fileL");
                    return (
                        <div style={{ display: "flex", gap: "20px" }}>
                            <div style={{ flex: "3", background: "#f0f0f0", padding: "10px" }}>
                                {(fileList ? fileList.length === 0 : true) ? (
                                    <p>Nessun file</p>
                                ) : (
                                    <ol>
                                        {fileList.map((file, index) => (
                                            <li key={index} style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                                                {file.file.name}
                                                <button onClick={() => handleRemoveFile(fileList, index, context.setFieldValue)}>Rimuovi</button>
                                            </li>
                                        ))}
                                    </ol>
                                )}
                            </div>
                            <div style={{ flex: "2", background: "#e0e0e0", padding: "10px" }}>
                                <FormElement accessor="fileL" />
                            </div>
                        </div>)
                }
                }

            </FormGeneratorContext.Consumer>
        </FormGeneratorContextProvider>
    </>
}
