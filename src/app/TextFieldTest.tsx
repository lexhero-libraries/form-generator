import React from "react";
import * as Yup from "yup";
import {FormElements} from "../form-generator/ElementInterface";
import FormDescriptor from "../form-generator/form-descriptor/FormDescriptor";
import FormGeneratorContextProvider from "../form-generator/form-context/FormGeneratorContextProvider";
import FormElement from "../form-generator/form-elements/FormElement";
import {FormGeneratorContext} from "../esm";


const companyFormElements:FormElements = [
    {
        accessor:"textField",
        type:"text",
        Header:"Text field"
    }
]
const initialValues =()=> {
    return {
        textField:Math.random(),
    }
}
const validationSchema = Yup.object().shape({
    textField: Yup.string().required('Text field').min(5,"troppo corto"),
})

function handleOnChange(e){
    if (e.value === 'ciao') {
        e.setFieldValue('Hola PIPPO')
    } else if (e.value === 'Hola PIPPO '){
        console.log(e.initialValue);
        e.reset()
    }else {
        e.defaultSetter()
    }
    
}

const formDescriptor = new FormDescriptor({elements:companyFormElements,initialValues, validationSchema})

export default function TextFieldTest(){
    return <>
        <FormGeneratorContextProvider validateOnChange onChange={(value)=>{}} formDescriptor={formDescriptor} onSubmit={()=>{}}>
            <FormGeneratorContext.Consumer>
                {()=>{
                    return <div>
                        <section className={"my-3"}>
                            <h3>Text element standard</h3>
                            <FormElement accessor="textField" onChange={handleOnChange} />
                        </section>

                        <button type="submit">SAVE</button>
                    </div>
                }}
            </FormGeneratorContext.Consumer>
        </FormGeneratorContextProvider>
    </>
}
