import {FormikErrors, FormikValues} from "formik";

export type SetFieldValue = (value:any)=> Promise<void> | Promise<FormikErrors<FormikValues>>
export default class OnChangeEventHandler {
    value : any
    accessorCurrentValue : any
    setFieldValue : SetFieldValue
    initialValue : any

    constructor(value: any, accessorCurrentValue: any, setFieldValue: SetFieldValue, initialValue: any) {
        this.value = value
        this.accessorCurrentValue = accessorCurrentValue
        this.setFieldValue = setFieldValue
        this.initialValue = initialValue
    }

    defaultSetter = () => this.setFieldValue(this.value)
    reset = () => this.setFieldValue(this.initialValue)
}