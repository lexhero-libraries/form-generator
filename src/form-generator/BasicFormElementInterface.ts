import ElementInterface from "./ElementInterface";
import {FormikErrors, FormikTouched, FormikValues} from "formik";
import React, { CSSProperties } from 'react'
import OnChangeEventHandler, {SetFieldValue} from "./events/OnChangeEventHandler";

export default interface BasicFormElementInterface extends ElementInterface{
    values:FormikValues,
    initialValues:FormikValues,
    className?:string,
    style?:CSSProperties,
    errors:FormikErrors<FormikValues>,
    touched:FormikTouched<FormikValues>,
    setFieldValue:SetFieldValue
    onChange?: (OnChangeEvent:OnChangeEventHandler)=> void,
    disable?:boolean,
    placeholder?:string,
    label?: string | false
    size?: "lg" | "sm"
}

export interface WithButtonElementInterface extends BasicFormElementInterface{
    addButton?:React.ReactHTMLElement<any>,
    removeButton?:React.ReactHTMLElement<any>,
}
