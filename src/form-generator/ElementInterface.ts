import {SelectOption} from "./form-elements/interfaces/SelectElementInterface";
import {RadioOption} from "./form-elements/interfaces/RadioElementInterface";
import {Rows} from "./form-elements/interfaces/CollectionElementInterface";
import { IStyleCustomization } from "react-dropzone-uploader";
import { CSSProperties } from "react";

export type ElementType  = "text" | "number" | "select" | "radio" | "checkbox" | "file" | "fileList" | "wysiwyg" | "tags" | "collection" | "embedded" | "date" | "countries" | "password" | "float" | "price" | "tel" | "dictionary"| "switch" | "form" | "textarea"
export type FileType = "*" | "image/*" | "application/*" | "application/pdf" | "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

export default interface ElementInterface{
    Header:string,
    accessor: string,
    type:ElementType,
}

export interface TextElementInterface extends ElementInterface{
    type:"text",
}

export interface TextareaElementInterface extends ElementInterface{
    type:"textarea",
}

export interface CheckboxElementInterface extends ElementInterface{
    type:"checkbox",
}

export interface RadioElementInterface extends ElementInterface{
    type:"radio",
    options:RadioOption[]
}

export interface TextElementInterface extends ElementInterface{
    type:"text",
}

export interface CountriesElementInterface extends ElementInterface{
    type:"countries",
}

export interface SelectElementInterface extends ElementInterface{
    type:"select",
    options:SelectOption[]
}

export interface WYSIWYGElementInterface extends ElementInterface{
    type:"wysiwyg",
    toolbar?:boolean
}

export interface TagsElementInterface extends ElementInterface{
    type:"tags",
    maxResults?:number
}

export interface NumberElementInterface extends ElementInterface{
    type:"number",
}

export interface DateElementInterface extends ElementInterface{
    type:"date",
}
export interface CollectionElementInterface extends ElementInterface{
    type:"collection",
    formElements:FormElements,
    initialValues: object,
    rows?: Rows,
    lockList?:boolean,
    hideSeparator?: boolean
}

export interface DictionaryElementInterface extends ElementInterface{
    type:"dictionary",
}

export interface EmbeddedElementInterface extends ElementInterface{
    type:"embedded",
    selectable?:boolean
    formElements:FormElements,
    initialValues: object,
}
export interface FileElementInterface extends ElementInterface{
    type:"file",
    accept: FileType,
    maxFileSizeMB?: number
}
export interface FileListElementInterface extends ElementInterface{
    type:"fileList"
    accept: FileType
    maxFiles?: number
    maxFileSizeMB?: number
    removeButton?:JSX.Element
    dropzoneContent?:JSX.Element
    label?: string | false
    showDropzone?:boolean
    showList?:boolean
    dropzoneClassNames?:IStyleCustomization<string>
    dropzoneStyles?:IStyleCustomization<CSSProperties>
    texts?:{
        remove?:string,
        upload?:string,
        error?:string
    }
}

export interface PasswordElementInterface extends ElementInterface{
    type:"password"
}

export interface FloatElementInterface extends ElementInterface{
    type:"float"
}

export interface PriceElementInterface extends ElementInterface{
    type:"price"
}

export interface TelephoneElementInterface extends ElementInterface{
    type:"tel"
}

export interface SwitchElementInterface extends ElementInterface{
    type:"switch"
}

export interface FormElementInterface extends ElementInterface{
    type:"form",
}



export type GenericElementInterface = TextElementInterface | SelectElementInterface | CheckboxElementInterface | RadioElementInterface | WYSIWYGElementInterface | TagsElementInterface|NumberElementInterface | CollectionElementInterface | EmbeddedElementInterface | FileElementInterface | FileListElementInterface | DateElementInterface | CountriesElementInterface | PasswordElementInterface |FloatElementInterface | PriceElementInterface | TelephoneElementInterface | DictionaryElementInterface | SwitchElementInterface | FormElementInterface | TextareaElementInterface
export type FormElements = GenericElementInterface[]
