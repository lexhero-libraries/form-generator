import BasicFormElementInterface from "../../BasicFormElementInterface";
import {FileType} from "../../ElementInterface";

export interface FileElementInterface extends BasicFormElementInterface{
    type:"file",
    accept: FileType,
    maxFileSizeMB?: number
    removeButton?:JSX.Element
    texts?:{
        remove:string,
        download:string,
        upload:string,
        error:string,
        noContent:string
    }
}
