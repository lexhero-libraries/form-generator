import {Button, Col, Row} from "react-bootstrap";
import React, {useContext} from "react";
import FormGeneratorContext from "../../../form-context/FormGeneratorContext";
import FormGeneratorContextProvider from "../../../form-context/FormGeneratorContextProvider";
import {getNestedValue} from "../../utils/form-generator-utils";
import {CollectionElementInterface, Rows} from "../../interfaces/CollectionElementInterface";
import FormDescriptor from "../../../form-descriptor/FormDescriptor";

export default function CollectionFormField({accessor, nestedForm, rows=1, addButton:addButtonProps, removeButton:removeButtonProps,initialValues:initialValuesList, lockList=false, hideSeparator=false}:CollectionElementInterface){
    const {setFieldValue, disable,values,errors,elements, formValue, unsetFieldValue} = useContext(FormGeneratorContext);
    const initialValues = (typeof initialValuesList === "object" || initialValuesList===undefined) ? initialValuesList: initialValuesList()
    const existingElements = getNestedValue(accessor,values)
    const showButton = (!disable && !lockList)

    const addButton = showButton && ( (addButtonProps) ?  React.cloneElement(addButtonProps,{onClick:(e)=>{e.preventDefault(); setFieldValue(`${accessor}[${existing}]`,initialValues)}}) : <Button type="button" onClick={(e)=>{e.preventDefault(); setFieldValue(`${accessor}[${existing}]`,initialValues)}}>+</Button>)
    const removeButton = (indexAccessor:string) => {
        return showButton && ( (removeButtonProps) ?  React.cloneElement(removeButtonProps,{onClick:() => unsetFieldValue(indexAccessor)}) : <Button onClick={() => unsetFieldValue(indexAccessor)}>-</Button>)
    }

    const collectionElement = elements.find(element => element.accessor ===accessor);
    const existing = getNestedValue(accessor,values).length
    // @ts-ignore
    const nestedElements= collectionElement.formElements
    const formDescriptor = new FormDescriptor({elements:nestedElements,initialValues})

    const suddivisioni = suddividiInGruppi(existingElements, rows)

    const nestedForms =  suddivisioni.map((gruppi:any[], gruppoIndex:number)=>{
        return <>
            <Row key={gruppoIndex} className={"mb-3"}>
            {gruppi.map((element:any,index:number)=> {
                const trueIndex = gruppoIndex*rows +index
                const indexAccessor = `${accessor}[${trueIndex}]`
                return <>
                    {showButton && <Col xs={1} className={"d-flex justify-content-center align-items-center"}>
                        {removeButton(indexAccessor)}
                    </Col>}
                    <Col xs={getColXS(rows, showButton)}>
                        <FormGeneratorContextProvider existingErrors={getNestedValue(indexAccessor, errors)} disable={disable} formValue={formValue} key={trueIndex} formDescriptor={formDescriptor} existingValue={getNestedValue(indexAccessor, values)}  accessorRoot={indexAccessor} onChange={(value) => {setFieldValue(indexAccessor, value)}} children={nestedForm ? nestedForm(trueIndex, element) : undefined}/>
                    </Col>
                </>
            })}
            </Row>
            {!hideSeparator && <hr/>}
        </>

    })

    if(collectionElement === undefined) return <div>{accessor}</div>
    return <>
        {nestedForms}
        {addButton}
    </>
}

function getColXS(rows:Rows, showButton:boolean){
    if(showButton){
        switch (rows){
            case 1: return 11;
            case 2: return 5;
            case 3: return 3;
            case 4: return 2;
        }
    }else{
        switch (rows){
            case 1: return 12;
            case 2: return 6;
            case 3: return 4;
            case 4: return 3;
        }
    }

}

function suddividiInGruppi(array:any[], dimensioneGruppo:number) {
    return array.reduce((acc, _, i) => {
        if (i % dimensioneGruppo === 0) {
            acc.push(array.slice(i, i + dimensioneGruppo));
        }
        return acc;
    }, []);
}

