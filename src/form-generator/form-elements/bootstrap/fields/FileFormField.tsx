import {Button, Form} from "react-bootstrap";
import React from "react";
import Dropzone, {IFileWithMeta} from "react-dropzone-uploader";
import DropzonePreview from "../../utils/DropzonePreview";
import {readFile} from "../../utils/FileUploadedHelper";
import 'react-dropzone-uploader/dist/styles.css'
import {getNestedValue} from "../../utils/form-generator-utils";
import {FileElementInterface} from "../../interfaces/FileElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function FileFormField(props: FileElementInterface) {
    const { type,
        values,
        disable,
        errors,
        touched,
        setFieldValue,
        accessor,
        Header,
        label,
        accept,
        maxFileSizeMB = 10,
        removeButton: removeFileButtonProps,
        texts = {
            remove: "Rimuovi file",
            download: "Scarica file",
            upload: "Carica file",
            error: "File troppo grande",
            noContent: "Nessun file caricato"
        }, onChange: myOnChange, initialValues } = props

    const defaultValue = undefined
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const existingFile = getNestedValue(accessor, values)
    const onDownloadFile = () => { window.open(process.env.REACT_APP_ENTRYPOINT + existingFile.url) }

    const removeFileButton = ((removeFileButtonProps) ? React.cloneElement(removeFileButtonProps, { onClick: (e: MouseEvent) => { e.preventDefault(); setFieldValue(null) } }) : <Button onClick={()=>{setFieldValue(null)}}>{texts.remove}</Button>)

    const onChangeHandler = (file:IFileWithMeta, result:unknown) => {
        if (myOnChange) {
            const value = { file, fileParsed: result }
            const myOnChangeE = new OnChangeEventHandler(value, existingFile, setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(result)
            myOnChangeE.reset = () => {
                file.remove()
                return setFieldValue(initialValue);
            }
            myOnChange(myOnChangeE);
        } else {
            setFieldValue(result)
        }
    }

    return <>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        {existingFile && <>
            {existingFile.url && <Button onClick={onDownloadFile}>{texts.download}</Button>}
            {!disable && removeFileButton}
        </>}
        {!existingFile && !disable && <>
            <Dropzone
                onSubmit={(successFiles) => {
                    const files = successFiles.map(file => file.file)
                }}
                onChangeStatus={(file, status, allFiles) => {
                    if (status === "error_file_size") {
                        throw new Error(texts.error)
                    }
                    if (status === "done") {
                        readFile(file.file).then(result => {onChangeHandler(file, result)})
                    }
                }}
                submitButtonDisabled={true}
                submitButtonContent={<button style={{ height: "0!important", width: "0!important" }} hidden={true} />}
                PreviewComponent={DropzonePreview}
                accept={accept}
                maxFiles={1}
                maxSizeBytes={maxFileSizeMB * 1024 * 1024}
                inputContent={texts.upload}
            />
            <p style={{ fontSize: 10 }}>Max File Size: {maxFileSizeMB}MB</p>
        </>}
        {!existingFile && disable && <div>{texts.noContent}</div>}
    </>
}
