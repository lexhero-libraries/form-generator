import { FileListElementInterface } from "../../interfaces/FileListElementInterface";
import useFileListFormField from "./hooks/useFileListFormField";

export default function FileListFormField(props: FileListElementInterface) {
    const { Label, FileList, DropFileZone } = useFileListFormField(props);

    return (
        <div style={props?.style} className={props?.className} >
            {Label}
            {FileList}
            {DropFileZone}
        </div>
    );
}
