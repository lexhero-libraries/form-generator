import React from "react";
// @ts-ignore
import DatePicker from "react-datepicker";
import { normalizeDate, serializeDate } from "../../utils/TimeManager";
import 'react-datepicker/dist/react-datepicker.css';
import { getNestedValue } from "../../utils/form-generator-utils";
import { Form } from "react-bootstrap";
import { DateElementInterface } from "../../interfaces/DateElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function DateFormField(props: DateElementInterface) {
    const { values, disable, errors, touched, setFieldValue, accessor, Header, label, placeholder, size, className, style, onChange: myOnChange, initialValues } = props

    const value = getNestedValue(accessor, values);
    const nestedError = getNestedValue(accessor, errors)
    const nestedTouched = getNestedValue(accessor, touched)
    const hasError = nestedTouched && nestedError !== undefined

    const defaultValue = null
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler = (value: any) => {
        if (myOnChange) {
            const valueAndMetod = { value, serializeDate }
            const myOnChangeE = new OnChangeEventHandler(valueAndMetod, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(serializeDate(value));
            myOnChange(myOnChangeE);
        } else {
            setFieldValue(serializeDate(value))
        }
    }

    return <div>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <DatePicker size={size} disabled={disable} placeholderText={placeholder} style={style} className={`form-control ${(className !== undefined ? className : '')}`} selected={value ? normalizeDate(value) : defaultValue} onChange={onChangeHandler} dateFormat={"dd/MM/yyyy"} />
        <span style={{ visibility: hasError ? "visible" : "hidden" }} className={"small text-danger"}>{nestedError ?? "error"}</span>
    </div>
}
