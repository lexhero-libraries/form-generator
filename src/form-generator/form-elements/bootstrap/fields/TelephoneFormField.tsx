import {Form} from "react-bootstrap";
import React from "react";
import {getNestedValue} from "../../utils/form-generator-utils";
import {PatternFormat} from 'react-number-format';
import {FormGroup} from "../../utils/FormGroup";
import {TelephoneElementInterface} from "../../interfaces/TelephoneElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function TelephoneFormField(props: TelephoneElementInterface) {
    const { type, values, errors, touched, setFieldValue, accessor, Header, label, className, style, onChange: myOnChange, initialValues } = props
    const errorMessage = getNestedValue(accessor, errors)
    const nestedTouched = getNestedValue(accessor, touched)

    const defaultValue = undefined
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler = (e: any) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChange(myOnChangeE);
        } else {
            setFieldValue(e.value)
        }
    }

    return <FormGroup>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <PatternFormat name={accessor} valueIsNumericString value={getNestedValue(accessor, values)} onValueChange={onChangeHandler} format="+## ##########" mask="_" style={style} className={`form-control ${(className !== undefined ? className : '')}`} allowEmptyFormatting />
        {nestedTouched && <div className="d-block">{errorMessage}</div>}
    </FormGroup>
}
