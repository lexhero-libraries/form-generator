import { Button, Form } from "react-bootstrap";
import React, { CSSProperties, useEffect, useState } from "react";
import Dropzone, { IFileWithMeta, IStyleCustomization } from "react-dropzone-uploader";
import DropzonePreview from "../../../utils/DropzonePreview";
import { readFile } from "../../../utils/FileUploadedHelper";
import 'react-dropzone-uploader/dist/styles.css'
import { getNestedValue } from "../../../utils/form-generator-utils";
import OnChangeEventHandler from "../../../../events/OnChangeEventHandler";
import { FileListElementInterface } from "../../../interfaces/FileListElementInterface";

export default function useFileListFormField(props: FileListElementInterface) {
    const {
        type,
        values,
        disable,
        errors,
        touched,
        setFieldValue,
        accessor,
        Header,
        label,
        accept,
        maxFiles = 1,
        maxFileSizeMB = 10,
        removeButton,
        texts,
        onChange: myOnChange,
        initialValues,
        showDropzone = true,
        showList = true,
        dropzoneClassNames,
        dropzoneStyles,
        dropzoneContent
    } = props

    const checkedTexts = {
        remove: texts?.remove ?? "Rimuovi file",
        upload: texts?.upload ?? "Carica file",
        error: texts?.error ?? "File troppo grande"
    }


    function initialValueCheck(params: any): any[] {
        if (Array.isArray(params)) return params;
        if (params !== undefined) console.error('initialValue non è un array')
        return [];
    }
    const initialValue = initialValueCheck(getNestedValue(accessor, initialValues));
    const existingFiles = getNestedValue(accessor, values) || [];

    const removeFile = (index: number) => {
        const newFiles = existingFiles.filter((_: any, i: number) => i !== index);
        setFieldValue(newFiles);
    };

    const onChangeHandler = (files: any[]) => {
        // todo se vuoi il base64 fai diventare la function async e decommenta
        // for (const i in files) {
        //     files[i].base64 = await readFile(files[i].file)
        // 
        const remainingSlots = maxFiles - existingFiles.length;
        const newFiles = files.slice(0, remainingSlots);
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(newFiles, existingFiles, setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue([...existingFiles , ...newFiles]);
            myOnChange(myOnChangeE);
        } else {
            setFieldValue([...existingFiles, ...newFiles]);
        }
    }

    const isMaxFilesReached = existingFiles.length >= maxFiles;
    const checkedDisable = disable !== undefined
        ? (disable ?? isMaxFilesReached)
        : isMaxFilesReached;

    const Label = label !== false && <Form.Label>{label ?? Header}</Form.Label>

    const FileList = showList && existingFiles.map(
        (file: any, index: number) => (
            <div key={index}>
                {file.file.name && <span>{file.file.name} </span>}
                <span style={{ cursor: 'pointer' }} onClick={() => removeFile(index)}>{removeButton ?? <Button>{checkedTexts.remove}</Button>
                }</span>
            </div>

        )
    )

    const DropFileZone = showDropzone && <>
        <Dropzone
            onSubmit={(successFiles) => {
                const files = successFiles.map(file => file.file)
            }}
            onChangeStatus={(file, status, allFiles) => {
                if (status === "error_file_size") {
                    throw new Error(checkedTexts.error)
                }
                if (status === "done") {

                    if (allFiles[allFiles.length - 1] === file) {
                        onChangeHandler([...allFiles])
                        const filesToRemove = [...allFiles];
                        filesToRemove.forEach((fileToRemove) => {
                            fileToRemove.remove();
                        });
                    }

                }
            }}
            submitButtonDisabled={true}
            submitButtonContent={<button style={{ height: "0!important", width: "0!important" }} hidden={true} />}
            PreviewComponent={DropzonePreview}
            accept={accept}
            maxFiles={maxFiles}
            maxSizeBytes={maxFileSizeMB * 1024 * 1024}
            inputContent={dropzoneContent ?? checkedTexts.upload}
            classNames={dropzoneClassNames}
            styles={dropzoneStyles}
            disabled={checkedDisable}
        />
        <p style={{ fontSize: 10, opacity: checkedDisable ? 0.5 : 1 }}>Max File Size: {maxFileSizeMB}MB</p>
    </>

    return {
        existingFiles,
        removeFile,
        setFieldValue,
        Label,
        FileList,
        DropFileZone,
    };
}
