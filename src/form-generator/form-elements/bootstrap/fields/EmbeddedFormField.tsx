import {Col, Row} from "react-bootstrap";
import React, {useContext, useMemo, useState} from "react";
import FormGeneratorContext from "../../../form-context/FormGeneratorContext";
import {getNestedValue} from "../../utils/form-generator-utils";
import FormGeneratorContextProvider from "../../../form-context/FormGeneratorContextProvider";
import {EmbeddedElementInterface} from "../../interfaces/EmbeddedElementInterface";
import FormDescriptor from "../../../form-descriptor/FormDescriptor";
import FormElement from "../../FormElement";
import Select from "react-select";
import {GenericElementInterface} from "../../../ElementInterface";

export default function EmbeddedFormField({accessor,nestedForm,initialValues, selectable= false}:EmbeddedElementInterface){
    const {setFieldValue,values,elements, formValue} = useContext(FormGeneratorContext);
    const existingElement = getNestedValue(accessor,values) ?? initialValues
    // @ts-ignore
    const embeddedElement = elements.find(element => element.accessor ===accessor);
    if(embeddedElement === undefined) throw new Error(`Non è stato definito il campo ${accessor} nel formDescriptor`)
    if(!("formElements" in embeddedElement)) throw new Error()
    const nestedElements= embeddedElement.formElements
    const formDescriptor = new FormDescriptor({elements:nestedElements,initialValues})
    const [selectOptions, setSelectOptions] = useState<GenericElementInterface[]>(nestedElements.filter(nestedElement => {
        return existingElement[nestedElement.accessor] === undefined
    }))
    const [visibleElements, setVisibleElements] = useState<GenericElementInterface[]>(nestedElements.filter(nestedElement => {
        return existingElement[nestedElement.accessor] !== undefined
    }))
    const [value, setValue] = useState<{label:string,value:string}|undefined>(undefined)

    const onChangeSelect = (el:any) => {
        const option = selectOptions.find(element => element.accessor === el.value)
        if(option){
            setVisibleElements([...visibleElements, option])
            setSelectOptions(selectOptions.filter(selectOption => selectOption !== option))
            setValue(undefined)
        }

    }

    if(selectable){
        return <Row className={"mb-3"}>
            <Col xs={12}>
                <Select isDisabled={selectOptions.length===0} inputValue={value?.value} classNamePrefix="react-select" value={value} onChange={onChangeSelect} options={selectOptions.map(element => {
                    return {label: element.Header, value:element.accessor}
                })} />
                <FormGeneratorContextProvider formDescriptor={formDescriptor} formValue={formValue} existingValue={existingElement} onChange={(value) => setFieldValue(accessor, value)}>
                    {visibleElements.map((formElement, index) => <React.Fragment key={index}>
                        <FormElement accessor={formElement.accessor}/>
                    </React.Fragment>)
                    }
                </FormGeneratorContextProvider>
            </Col>
        </Row>
    }

    const nestedForms = useMemo(()=>{
        return (<Row className={"mb-3"}>
            <Col xs={12}>
                <FormGeneratorContextProvider formDescriptor={formDescriptor} children={nestedForm ? nestedForm(1) : undefined} formValue={formValue} existingValue={existingElement} onChange={(value) => setFieldValue(accessor, value)} />
            </Col>
        </Row>)
    },[existingElement, accessor, initialValues])

    return <>
        {nestedForms}
    </>
}
