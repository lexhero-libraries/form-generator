import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Form} from "react-bootstrap";
import ReactQuill from "react-quill";
import 'react-quill/dist/quill.snow.css';
import {getNestedValue} from "../../utils/form-generator-utils";
import {WYSIWYGElementInterface} from "../../interfaces/WYSIWYGElementInterface";
import {useDebouncedCallback} from "use-debounce";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function WYSIWYGFormField(props:WYSIWYGElementInterface){
    const {label,className,type,values, errors, touched,setFieldValue,accessor,Header, toolbar, style, onChange: myOnChange, initialValues } = props
    const formValue =useMemo(()=>getNestedValue(accessor,values),[accessor,values])
    const [value, setValue] = useState<string|undefined>(formValue);

    const updateValueForm = useCallback(()=>{
        if(value !== formValue){
            setFieldValue(value)
        }
    },[value, formValue, accessor])

    const updateFromForm = useCallback(()=>{
        if(value !== formValue){
            setValue(formValue)
        }
    },[value,formValue])

    const debouncedDataHandler = useDebouncedCallback(()=>{
        updateFromForm()
    },500)

    useEffect(()=>{
        debouncedDataHandler()
    },[values])
    useEffect(()=>{updateValueForm()},[value])

    const modules = {
        history: {
            delay: 2000,
            maxStack: 500,
            userOnly: true
        },
        toolbar: (toolbar !== undefined ? toolbar : true)
    }

    const defaultValue = "<p><br></p>"
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler= (content:any) => {
        const errorSetFieldValue =(value:any)=>{
            console.error('Non è la maniera giusta per modificare il testo. Ha ancora dei bug da sistemare su form-generator');
            return setFieldValue(value)
        };

        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(content, getNestedValue(accessor, values), errorSetFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => { setValue(content); return content}
            myOnChangeE.reset = () => { setValue(initialValue); return content}
            myOnChange(myOnChangeE);
        } else {
            setValue(content)
        }
    }

    return <>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <ReactQuill style={style} className={`form-group ${(className !== undefined ? className : '')}`} modules={modules} theme="snow" value={value} onChange={onChangeHandler}/>
    </>
}
