import {Form} from "react-bootstrap";
import React from "react";
import {getNestedValue} from "../../utils/form-generator-utils";
import {FormGroup} from "../../utils/FormGroup";
import {PasswordElementInterface} from "../../interfaces/PasswordElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function PasswordFormField(props: PasswordElementInterface) {
    const { type, disable, values, errors, touched, setFieldValue, accessor, Header, size, label, className, style, onChange: myOnChange, initialValues } = props
    const nestedError = getNestedValue(accessor, errors)
    const nestedTouched = getNestedValue(accessor, touched)

    const defaultValue = ""
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.target.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChange(myOnChangeE);
        } else {
            setFieldValue(e.target.value)
        }
    }

    return <FormGroup>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <Form.Control style={style} className={className} size={size} isInvalid={nestedError !== undefined} disabled={disable} type="password" name={accessor} placeholder={Header} value={getNestedValue(accessor, values) ?? defaultValue} onChange={onChangeHandler} />
        <Form.Control.Feedback
            className="font-weight-bold"
            type="invalid"
            role="alert"
            aria-label="from feedback"
            tooltip
        >
            {nestedError}
        </Form.Control.Feedback>
    </FormGroup>
}
