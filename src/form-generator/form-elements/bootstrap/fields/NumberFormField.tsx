import { Form } from "react-bootstrap";
import React from "react";
import { getNestedValue } from "../../utils/form-generator-utils";
import { FormGroup } from "../../utils/FormGroup";
import { NumberElementInterface } from "../../interfaces/NumberElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";


export default function NumberFormField(props: NumberElementInterface) {
    const { type, className, values, errors, touched, setFieldValue, accessor, Header, placeholder, label, size, style, onChange: myOnChange, initialValues } = props
    const nestedError = getNestedValue(accessor, errors)
    const nestedTouched = getNestedValue(accessor, touched)

    const defaultValue = ""
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.target.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(parseInt(e.target.value));
            myOnChange(myOnChangeE);
        } else {
            setFieldValue(parseInt(e.target.value))
        }
    }

    return <FormGroup>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <Form.Control style={style} className={className} size={size} isInvalid={nestedError !== undefined} type="number" name={accessor} placeholder={placeholder} value={getNestedValue(accessor, values) ?? defaultValue} onChange={onChangeHandler} />
        <Form.Control.Feedback
            className="font-weight-bold"
            type="invalid"
            role="alert"
            aria-label="from feedback"
            tooltip
        >
            {nestedError}
        </Form.Control.Feedback>
    </FormGroup>
}
