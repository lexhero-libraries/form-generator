import {Form} from "react-bootstrap";
import React from "react";
import {FormGroup} from "../../utils/FormGroup";
import {getNestedValue} from "../../utils/form-generator-utils";
import {FloatElementInterface} from "../../interfaces/FloatElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";


export default function FloatFormField(props: FloatElementInterface) {
    const { type, values, errors, touched, setFieldValue, accessor, Header, size, label, className, style, onChange: myOnChange, initialValues } = props
    const nestedError = getNestedValue(accessor, errors)
    const nestedTouched = getNestedValue(accessor, touched)

    const defaultValue = ""
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.target.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(parseFloat(e.target.value));
            myOnChange(myOnChangeE);
        } else {
            setFieldValue(parseFloat(e.target.value))
        }
    }

    return <FormGroup>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <Form.Control style={style} className={className} size={size} isInvalid={nestedError !== undefined} type="number" name={accessor} placeholder={Header} value={values[accessor]} onChange={onChangeHandler} />
        <Form.Control.Feedback
            className="font-weight-bold"
            type="invalid"
            role="alert"
            aria-label="from feedback"
            tooltip
        >
            {nestedError}
        </Form.Control.Feedback>
    </FormGroup>
}
