import { Form } from "react-bootstrap";
import React from "react";
import { getNestedValue } from "../../utils/form-generator-utils";
import { FormGroup } from "../../utils/FormGroup";
import { PriceElementInterface } from "../../interfaces/PriceElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function PriceFormField(props: PriceElementInterface) {
    const { type, values, errors, touched, setFieldValue, accessor, Header, size, label, className, style, onChange: myOnChange, initialValues } = props
    const nestedError = getNestedValue(accessor, errors)
    const nestedTouched = getNestedValue(accessor, touched)
    const nestedValue = getNestedValue(accessor, values)

    const defaultValue = 0
    const initialValue = getNestedValue(accessor, initialValues) ? getNestedValue(accessor, initialValues) / 100 : defaultValue
    const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.target.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue((parseFloat(e.target.value)) * 100)
            myOnChange(myOnChangeE);
        } else {
            setFieldValue((parseFloat(e.target.value)) * 100)
        }
    }

    return <FormGroup>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <Form.Control style={style} className={className} size={size} isInvalid={nestedError !== undefined} type="number" name={accessor} placeholder={Header} value={nestedValue / 100} onChange={onChangeHandler} />
        <Form.Control.Feedback
            className="font-weight-bold"
            type="invalid"
            role="alert"
            aria-label="from feedback"
            tooltip
        >
            {nestedError}
        </Form.Control.Feedback>
    </FormGroup>
}
