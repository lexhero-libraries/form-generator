import React from "react";
import {Form} from "react-bootstrap";
import {getNestedValue} from "../../utils/form-generator-utils";
import {FormGroup} from "../../utils/FormGroup";
import {CheckboxElementInterface} from "../../interfaces/CheckboxElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function CheckboxFormField(props:CheckboxElementInterface){
    const {type,values, errors, touched,setFieldValue,accessor,Header, className, style, onChange: myOnChange, initialValues } = props
    const nestedError = getNestedValue(accessor,errors)
    const nestedTouched = getNestedValue(accessor,touched)

    const defaultValue = false
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) => {
            if(myOnChange){
                const myOnChangeE = new OnChangeEventHandler(e.target.checked, getNestedValue(accessor, values), setFieldValue,initialValue);
                myOnChangeE.defaultSetter = () => setFieldValue(!getNestedValue(accessor,values))
                myOnChange(myOnChangeE);
            }else{
                setFieldValue(!getNestedValue(accessor,values))
            }
        }

    return <FormGroup>
        <Form.Check  style={style} className={className} name={accessor} type="checkbox" label={Header} id={accessor} onChange={onChangeHandler} checked={getNestedValue(accessor,values)?? defaultValue}
                    isInvalid={nestedError!==undefined}
                    feedback={nestedError}
                    feedbackType="invalid"
                    feedbackTooltip
        />
    </FormGroup>
}
