import {Form} from "react-bootstrap";
import React from "react";
import {getNestedValue} from "../../utils/form-generator-utils";
import {TextElementInterface} from "../../interfaces/TextElementInterface";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";

export default function TextFormField(props: TextElementInterface) {
    const { disable, values, size, errors, touched, setFieldValue, accessor, Header, placeholder, label, className, style, onChange: myOnChange, initialValues } = props
    const nestedError = getNestedValue(accessor, errors)
    
    const defaultValue = ""
    const initialValue = getNestedValue(accessor, initialValues) ?? defaultValue
    const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) => {
        if(myOnChange){
            const myOnChangeE = new OnChangeEventHandler(e.target.value, getNestedValue(accessor, values), setFieldValue,initialValue);
            myOnChange(myOnChangeE);
        }else{
            setFieldValue(e.target.value)
        }
    }

    return <Form.Group as={"div"} style={{ position: "relative" }}>
        {label !== false && <Form.Label>{label ?? Header}</Form.Label>}
        <Form.Control style={style} className={className} size={size} isInvalid={nestedError !== undefined} disabled={disable} type="text" name={accessor} placeholder={placeholder} value={getNestedValue(accessor, values) ?? defaultValue} onChange={onChangeHandler} />
        <Form.Control.Feedback
            className="font-weight-bold"
            type="invalid"
            role="alert"
            aria-label="from feedback"
            tooltip
        >
            {nestedError}
        </Form.Control.Feedback>
    </Form.Group>
}
