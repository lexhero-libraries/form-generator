import React, {CSSProperties, useContext} from "react";
import FormGeneratorContext from "../form-context/FormGeneratorContext";
import {GenericElementInterface} from "../ElementInterface";
import {getAccessorElementsNoIndex, getNestedValue} from "./utils/form-generator-utils";
import {SelectOption} from "./interfaces/SelectElementInterface";
import {RadioOption} from "./interfaces/RadioElementInterface";
import FormElementGenerator from "./FormElementGenerator";
import OnChangeEventHandler from "../events/OnChangeEventHandler";


interface FormElementInterface {
    accessor:string,
    onChange?:(OnChangeEvent:OnChangeEventHandler)=> void,
    className?:string,
    style?:CSSProperties,
    nestedForm?:(index:number, element: any)=>JSX.Element,
    placeholder?:string,
    label?: string | false,
    options?:SelectOption[] | RadioOption[],
    addButton?:any,
    removeButton?:any,
    disable?:boolean,
    size?:"sm"|"lg",
    texts?:{
        remove:string,
        download:string,
        upload:string,
        error:string,
        noContent:string
    }
}

function getElement(elements: GenericElementInterface[], accessorParsed: string[]):GenericElementInterface {
    if(accessorParsed.length <= 0) throw new Error("Accessore non valido.")
    const piece = elements.find(formElement => formElement.accessor === accessorParsed[0]);
    if(piece ===undefined) throw new Error(`Elemento ${accessorParsed[0]} non trovato nel formDescriptor.`)
    if(accessorParsed.length === 1) return piece;
    if("formElements" in piece ) return getElement(piece.formElements, accessorParsed.slice(1))
    throw new Error("Errore di configurazione del formElements annidato.")
}

export default function FormElement({accessor,nestedForm, options,...others}:FormElementInterface){
    const {values,errors,touched,setFieldValue,elements, formValue, initialValues} = useContext(FormGeneratorContext)
    const accessorParsed = getAccessorElementsNoIndex(accessor)
    const element = getElement(elements,accessorParsed);

    const finalOptions = getElementOptions(options, element)
    if(element){
        // @ts-ignore
        return <FormElementGenerator nestedForm={nestedForm} {...element} {...others} values={values} initialValues={initialValues} errors={errors} touched={touched} setFieldValue={(value) => setFieldValue(accessor, value)} accessor={accessor} options={finalOptions} />
    }
    return <div>{accessor}</div>

}

export function useFormElementValue(accessor:string){
    const {values,errors,touched,setFieldValue,elements,disable} = useContext(FormGeneratorContext)
    return getNestedValue(accessor,values)
}

function getElementOptions(options:SelectOption[]|RadioOption[]|undefined,element:GenericElementInterface) {
    if(options)return options
    if("options" in element) return element.options
    return undefined;
}
