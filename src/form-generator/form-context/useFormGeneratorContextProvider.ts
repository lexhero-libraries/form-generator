import {FormGeneratorContextProviderProps} from "./FormGeneratorContextProvider";
import {FormikValues, useFormik} from "formik";
import {useCallback, useEffect, useMemo} from "react";
import {isArrayElementAccessor} from "../form-elements/utils/form-generator-utils";

export default function useFormGeneratorContextProvider(props:FormGeneratorContextProviderProps){
    const {formValue, formDescriptor, validateOnChange=false, disable=false ,onSubmit, children, existingValue,existingErrors, accessorRoot, onChange} = props
    const {elements,validationSchema,initialValues:initialValuesFormDescriptor} = formDescriptor
    const initialValues = (typeof initialValuesFormDescriptor === "object" || initialValuesFormDescriptor===undefined) ? initialValuesFormDescriptor: initialValuesFormDescriptor()
    const onSubmitHandler = (values:FormikValues) => {
        if(onSubmit){
            const onSubmitResponse = onSubmit(values)
            if(onSubmitResponse instanceof Promise){
                return onSubmitResponse
                .then((response)=>{
                    if (formDescriptor.resetOnSubmit) {
                        resetForm();
                    }
                    return response;
                })
                .catch((error)=>{
                    if (error.response.status===400) {
                        //const response = GenericResponse.fromResponse(error.response)
                        setErrors(error.response);
                    }
                    throw error
                })
            }else{
                if (formDescriptor.resetOnSubmit) {
                    resetForm();
                }
                return onSubmitResponse;
            }
            
        }

        return new Promise<any>(()=>{});
    }

    const formik = useFormik({ initialValues, validationSchema, validateOnChange:validateOnChange, onSubmit:onSubmitHandler });
    const { handleSubmit, setValues,resetForm, values, touched, validateForm,errors, setFieldValue,setErrors, isValid,setTouched,submitForm,isValidating,isSubmitting } = formik;

    const updateValues = useCallback(()=>{
        if(existingValue && existingValue !== values) {
            setValues(existingValue)
        }
    },[existingValue,values])

    const updateWhenValuesChange = useCallback(()=>{
        if(values!==initialValues){
            if(onChange && values && values!==existingValue){
                validateForm(values).then(response => {
                    if(Object.keys(response).length ===0) onChange(values)
                })
            }
        }
    },[onChange ,values,isValidating, existingValue,initialValues])

    useEffect(()=>{
        updateWhenValuesChange()
    },[values])

    useEffect(()=>{
        updateValues()
    },[existingValue])

    const updateErrors = useCallback(()=>{
        if(existingErrors && existingErrors !== errors) {
            setErrors(existingErrors)
        }
    },[existingErrors,errors])

    useEffect(()=>{
        updateErrors()
    },[existingErrors])

    const unsetFieldValue = (accessor:string) => {
        if(isArrayElementAccessor(accessor)){
            const arrayAccessorStartingPosition = accessor.lastIndexOf("[");
            if(arrayAccessorStartingPosition !==-1){
                const indexToRemove = Number.parseInt(accessor.slice(arrayAccessorStartingPosition).slice(1,-1));
                const collectionAccessor = accessor.slice(0,arrayAccessorStartingPosition);
                // @ts-ignore
                const array:any[] = values[collectionAccessor];
                const newArray = array.filter((item,index) => index !== indexToRemove )
                const newValues = {...values}
                // @ts-ignore
                newValues[collectionAccessor] = newArray;
                setValues(newValues)
            }
        }else{
            const newValues = {...values}
            // @ts-ignore
            delete newValues[accessor]
            setValues(newValues)
        }

    }

    const getFieldValue = (accessor:string) => {
        if (typeof values === 'object') {
            // @ts-ignore
            return values?.[accessor]
        } 
        return undefined
        
    }

    return {validateForm,formValue:values, disable, values,errors, touched, setFieldValue, getFieldValue, unsetFieldValue, elements, submitForm, isValid,isValidating,isSubmitting,setErrors, onSubmit, handleSubmit}
}
