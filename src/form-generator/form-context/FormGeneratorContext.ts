import {FormikErrors, FormikTouched, FormikValues} from "formik";
import {FormElements, GenericElementInterface} from "../ElementInterface";
import React from "react";

export interface FormContextInterface{
    values:FormikValues,
    initialValues:FormikValues,
    formValue:FormikValues,
    errors:FormikErrors<FormikValues>,
    touched:FormikTouched<FormikValues>,
    setFieldValue:(name:string,value:any)=> Promise<void> | Promise<FormikErrors<FormikValues>>,
    getFieldValue:(accessor: string)=>any,
    unsetFieldValue: (accessor:string) => void
    elements:GenericElementInterface[],
    submitForm:(e?: React.FormEvent<HTMLFormElement> | undefined) => Promise<any>
    validateForm:  (values?: object | undefined) => Promise<object>
    disable:boolean,
    isValid:boolean,
    isValidating:boolean,
    isSubmitting:boolean
    setErrors:(errors: FormikErrors<any>) => void
}

const formContextDefaultValue:FormContextInterface = {
    values:{},
    initialValues:{},
    formValue:{},
    errors:{},
    touched:{},
    setFieldValue:(name:string,value:any)=>new Promise<void>(()=>{}),
    getFieldValue:(accessor:string)=>{},
    unsetFieldValue:(name:string)=>{},
    elements:[],
    submitForm: (e?: React.FormEvent<HTMLFormElement> | undefined) => new Promise<void>(()=>{}),
    validateForm:  (values?: object | undefined) => new Promise<object>(()=>{}),
    disable:false,
    isValid:false,
    isValidating:false,
    isSubmitting:false,
    setErrors: (errors:any) => {}
}


const FormGeneratorContext = React.createContext(formContextDefaultValue)

export default FormGeneratorContext
