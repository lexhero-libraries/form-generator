import {FormikErrors, FormikTouched, FormikValues} from "formik";
import React from "react";

import {FormElements} from "../ElementInterface";
import FormElement from "../form-elements/FormElement";
import FormGeneratorContext from "./FormGeneratorContext";
import FormButtonGenerator from "../form-button/FormButtonGenerator";
import FormDescriptor from "../form-descriptor/FormDescriptor";
import useFormGeneratorContextProvider from "./useFormGeneratorContextProvider";

type ConditionalProps = {
    accessorRoot?: string;
    onSubmit?: never;
    onChange: (value: any) => Promise<void> | Promise<FormikErrors<FormikValues>> | void
} | {
    accessorRoot?: never;
    onSubmit?: (values:any) => void | Promise<any>;
    onChange?: never;
};

type CommonProps = {
    accessorRoot?:string,
    onSubmit?: (values:any) => void | Promise<any>;
    onChange?: (value: any) => Promise<void> | Promise<FormikErrors<FormikValues>> | void
    children?:any,
    formDescriptor:FormDescriptor
    existingValue?:FormikValues,
    existingErrors?: FormikErrors<FormikValues>|undefined
    existingTouched?: FormikTouched<FormikValues>|undefined,
    formValue?: FormikValues|undefined,
    disable?: boolean
    validateOnChange?: boolean
}

export type FormGeneratorContextProviderProps = CommonProps

export default function FormGeneratorContextProvider(props:FormGeneratorContextProviderProps){
    const {validateForm,formValue, onSubmit, handleSubmit, disable, values,errors, touched, setFieldValue, getFieldValue, unsetFieldValue, elements, submitForm, isValid,isValidating,isSubmitting,setErrors} = useFormGeneratorContextProvider(props)
    
    let initialValues: any = {}
    if (typeof props.formDescriptor.initialValues === 'function') {
        initialValues = props.formDescriptor.initialValues()
    } else {
        initialValues = props.formDescriptor.initialValues
    }
    
    return <FormGeneratorContext.Provider value={{validateForm,formValue, disable, values, initialValues, errors, touched, setFieldValue, getFieldValue, unsetFieldValue, elements, submitForm, isValid,isValidating,isSubmitting,setErrors}}>
        <FormContent onSubmit={onSubmit} formElements={elements} handleSubmit={handleSubmit} children={props.children} />
    </FormGeneratorContext.Provider>
}

interface FormContentInterface{
    children?:any,
    onSubmit?:(values:any) => void | Promise<any>,
    formElements:FormElements,
    handleSubmit:any
}

// @ts-ignore
const FormContent = ({children,onSubmit,formElements,handleSubmit}:FormContentInterface) => {
    const button = onSubmit && <FormButtonGenerator/>
    const content = (children) ?? <FormGeneratorContext.Consumer>
        {()=>{
            return <>
                {formElements.map((formElement, index) => <React.Fragment key={index}>
                    <FormElement accessor={formElement.accessor}/>
                </React.Fragment>)
                }
                {button}
            </>
        }}
    </FormGeneratorContext.Consumer>

    return (onSubmit) ? <form noValidate onSubmit={handleSubmit}>{content}</form> : <>{content}</>
}
