/// <reference types="react" />
import { FormGeneratorContextProviderProps } from "./FormGeneratorContextProvider";
export default function useFormGeneratorContextProvider(props: FormGeneratorContextProviderProps): {
    validateForm: (values?: object | undefined) => Promise<object>;
    formValue: object;
    disable: boolean;
    values: object;
    errors: object;
    touched: object;
    setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => Promise<void> | Promise<object>;
    getFieldValue: (accessor: string) => any;
    unsetFieldValue: (accessor: string) => void;
    elements: import("../ElementInterface").FormElements;
    submitForm: () => Promise<any>;
    isValid: boolean;
    isValidating: boolean;
    isSubmitting: boolean;
    setErrors: (errors: object) => void;
    onSubmit: ((values: any) => void | Promise<any>) | undefined;
    handleSubmit: (e?: import("react").FormEvent<HTMLFormElement> | undefined) => void;
};
