import { jsx as _jsx, Fragment as _Fragment, jsxs as _jsxs } from "react/jsx-runtime";
import React from "react";
import FormElement from "../form-elements/FormElement";
import FormGeneratorContext from "./FormGeneratorContext";
import FormButtonGenerator from "../form-button/FormButtonGenerator";
import useFormGeneratorContextProvider from "./useFormGeneratorContextProvider";
export default function FormGeneratorContextProvider(props) {
    const { validateForm, formValue, onSubmit, handleSubmit, disable, values, errors, touched, setFieldValue, getFieldValue, unsetFieldValue, elements, submitForm, isValid, isValidating, isSubmitting, setErrors } = useFormGeneratorContextProvider(props);
    let initialValues = {};
    if (typeof props.formDescriptor.initialValues === 'function') {
        initialValues = props.formDescriptor.initialValues();
    }
    else {
        initialValues = props.formDescriptor.initialValues;
    }
    return _jsx(FormGeneratorContext.Provider, Object.assign({ value: { validateForm, formValue, disable, values, initialValues, errors, touched, setFieldValue, getFieldValue, unsetFieldValue, elements, submitForm, isValid, isValidating, isSubmitting, setErrors } }, { children: _jsx(FormContent, { onSubmit: onSubmit, formElements: elements, handleSubmit: handleSubmit, children: props.children }, void 0) }), void 0);
}
// @ts-ignore
const FormContent = ({ children, onSubmit, formElements, handleSubmit }) => {
    var _a;
    const button = onSubmit && _jsx(FormButtonGenerator, {}, void 0);
    const content = (_a = (children)) !== null && _a !== void 0 ? _a : _jsx(FormGeneratorContext.Consumer, { children: () => {
            return _jsxs(_Fragment, { children: [formElements.map((formElement, index) => _jsx(React.Fragment, { children: _jsx(FormElement, { accessor: formElement.accessor }, void 0) }, index)), button] }, void 0);
        } }, void 0);
    return (onSubmit) ? _jsx("form", Object.assign({ noValidate: true, onSubmit: handleSubmit }, { children: content }), void 0) : _jsx(_Fragment, { children: content }, void 0);
};
