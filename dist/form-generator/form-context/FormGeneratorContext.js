import React from "react";
const formContextDefaultValue = {
    values: {},
    initialValues: {},
    formValue: {},
    errors: {},
    touched: {},
    setFieldValue: (name, value) => new Promise(() => { }),
    getFieldValue: (accessor) => { },
    unsetFieldValue: (name) => { },
    elements: [],
    submitForm: (e) => new Promise(() => { }),
    validateForm: (values) => new Promise(() => { }),
    disable: false,
    isValid: false,
    isValidating: false,
    isSubmitting: false,
    setErrors: (errors) => { }
};
const FormGeneratorContext = React.createContext(formContextDefaultValue);
export default FormGeneratorContext;
