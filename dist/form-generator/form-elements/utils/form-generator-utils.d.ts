export declare const getAccessorElementsNoIndex: (accessor: string) => string[];
export declare const getAccessorElements: (accessor: string) => string[];
export declare const getNestedValue: (accessor: string, obj: any) => any;
export declare const isArrayElementAccessor: (accessor: string) => boolean;
