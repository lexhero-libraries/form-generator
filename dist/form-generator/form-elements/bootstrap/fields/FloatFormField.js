import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import { Form } from "react-bootstrap";
import { FormGroup } from "../../utils/FormGroup";
import { getNestedValue } from "../../utils/form-generator-utils";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";
export default function FloatFormField(props) {
    var _a;
    const { type, values, errors, touched, setFieldValue, accessor, Header, size, label, className, style, onChange: myOnChange, initialValues } = props;
    const nestedError = getNestedValue(accessor, errors);
    const nestedTouched = getNestedValue(accessor, touched);
    const defaultValue = "";
    const initialValue = (_a = getNestedValue(accessor, initialValues)) !== null && _a !== void 0 ? _a : defaultValue;
    const onChangeHandler = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.target.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(parseFloat(e.target.value));
            myOnChange(myOnChangeE);
        }
        else {
            setFieldValue(parseFloat(e.target.value));
        }
    };
    return _jsxs(FormGroup, { children: [label !== false && _jsx(Form.Label, { children: label !== null && label !== void 0 ? label : Header }, void 0), _jsx(Form.Control, { style: style, className: className, size: size, isInvalid: nestedError !== undefined, type: "number", name: accessor, placeholder: Header, value: values[accessor], onChange: onChangeHandler }, void 0), _jsx(Form.Control.Feedback, Object.assign({ className: "font-weight-bold", type: "invalid", role: "alert", "aria-label": "from feedback", tooltip: true }, { children: nestedError }), void 0)] }, void 0);
}
