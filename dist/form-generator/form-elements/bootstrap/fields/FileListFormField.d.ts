import { FileListElementInterface } from "../../interfaces/FileListElementInterface";
export default function FileListFormField(props: FileListElementInterface): JSX.Element;
