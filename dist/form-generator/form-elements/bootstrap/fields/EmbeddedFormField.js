import { jsx as _jsx, jsxs as _jsxs, Fragment as _Fragment } from "react/jsx-runtime";
import { Col, Row } from "react-bootstrap";
import React, { useContext, useMemo, useState } from "react";
import FormGeneratorContext from "../../../form-context/FormGeneratorContext";
import { getNestedValue } from "../../utils/form-generator-utils";
import FormGeneratorContextProvider from "../../../form-context/FormGeneratorContextProvider";
import FormDescriptor from "../../../form-descriptor/FormDescriptor";
import FormElement from "../../FormElement";
import Select from "react-select";
export default function EmbeddedFormField({ accessor, nestedForm, initialValues, selectable = false }) {
    var _a;
    const { setFieldValue, values, elements, formValue } = useContext(FormGeneratorContext);
    const existingElement = (_a = getNestedValue(accessor, values)) !== null && _a !== void 0 ? _a : initialValues;
    // @ts-ignore
    const embeddedElement = elements.find(element => element.accessor === accessor);
    if (embeddedElement === undefined)
        throw new Error(`Non è stato definito il campo ${accessor} nel formDescriptor`);
    if (!("formElements" in embeddedElement))
        throw new Error();
    const nestedElements = embeddedElement.formElements;
    const formDescriptor = new FormDescriptor({ elements: nestedElements, initialValues });
    const [selectOptions, setSelectOptions] = useState(nestedElements.filter(nestedElement => {
        return existingElement[nestedElement.accessor] === undefined;
    }));
    const [visibleElements, setVisibleElements] = useState(nestedElements.filter(nestedElement => {
        return existingElement[nestedElement.accessor] !== undefined;
    }));
    const [value, setValue] = useState(undefined);
    const onChangeSelect = (el) => {
        const option = selectOptions.find(element => element.accessor === el.value);
        if (option) {
            setVisibleElements([...visibleElements, option]);
            setSelectOptions(selectOptions.filter(selectOption => selectOption !== option));
            setValue(undefined);
        }
    };
    if (selectable) {
        return _jsx(Row, Object.assign({ className: "mb-3" }, { children: _jsxs(Col, Object.assign({ xs: 12 }, { children: [_jsx(Select, { isDisabled: selectOptions.length === 0, inputValue: value === null || value === void 0 ? void 0 : value.value, classNamePrefix: "react-select", value: value, onChange: onChangeSelect, options: selectOptions.map(element => {
                            return { label: element.Header, value: element.accessor };
                        }) }, void 0), _jsx(FormGeneratorContextProvider, Object.assign({ formDescriptor: formDescriptor, formValue: formValue, existingValue: existingElement, onChange: (value) => setFieldValue(accessor, value) }, { children: visibleElements.map((formElement, index) => _jsx(React.Fragment, { children: _jsx(FormElement, { accessor: formElement.accessor }, void 0) }, index)) }), void 0)] }), void 0) }), void 0);
    }
    const nestedForms = useMemo(() => {
        return (_jsx(Row, Object.assign({ className: "mb-3" }, { children: _jsx(Col, Object.assign({ xs: 12 }, { children: _jsx(FormGeneratorContextProvider, { formDescriptor: formDescriptor, children: nestedForm ? nestedForm(1) : undefined, formValue: formValue, existingValue: existingElement, onChange: (value) => setFieldValue(accessor, value) }, void 0) }), void 0) }), void 0));
    }, [existingElement, accessor, initialValues]);
    return _jsx(_Fragment, { children: nestedForms }, void 0);
}
