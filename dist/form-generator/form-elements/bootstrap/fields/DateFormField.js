import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
// @ts-ignore
import DatePicker from "react-datepicker";
import { normalizeDate, serializeDate } from "../../utils/TimeManager";
import 'react-datepicker/dist/react-datepicker.css';
import { getNestedValue } from "../../utils/form-generator-utils";
import { Form } from "react-bootstrap";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";
export default function DateFormField(props) {
    var _a;
    const { values, disable, errors, touched, setFieldValue, accessor, Header, label, placeholder, size, className, style, onChange: myOnChange, initialValues } = props;
    const value = getNestedValue(accessor, values);
    const nestedError = getNestedValue(accessor, errors);
    const nestedTouched = getNestedValue(accessor, touched);
    const hasError = nestedTouched && nestedError !== undefined;
    const defaultValue = null;
    const initialValue = (_a = getNestedValue(accessor, initialValues)) !== null && _a !== void 0 ? _a : defaultValue;
    const onChangeHandler = (value) => {
        if (myOnChange) {
            const valueAndMetod = { value, serializeDate };
            const myOnChangeE = new OnChangeEventHandler(valueAndMetod, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(serializeDate(value));
            myOnChange(myOnChangeE);
        }
        else {
            setFieldValue(serializeDate(value));
        }
    };
    return _jsxs("div", { children: [label !== false && _jsx(Form.Label, { children: label !== null && label !== void 0 ? label : Header }, void 0), _jsx(DatePicker, { size: size, disabled: disable, placeholderText: placeholder, style: style, className: `form-control ${(className !== undefined ? className : '')}`, selected: value ? normalizeDate(value) : defaultValue, onChange: onChangeHandler, dateFormat: "dd/MM/yyyy" }, void 0), _jsx("span", Object.assign({ style: { visibility: hasError ? "visible" : "hidden" }, className: "small text-danger" }, { children: nestedError !== null && nestedError !== void 0 ? nestedError : "error" }), void 0)] }, void 0);
}
