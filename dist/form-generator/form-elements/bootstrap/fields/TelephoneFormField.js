import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import { Form } from "react-bootstrap";
import { getNestedValue } from "../../utils/form-generator-utils";
import { PatternFormat } from 'react-number-format';
import { FormGroup } from "../../utils/FormGroup";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";
export default function TelephoneFormField(props) {
    var _a;
    const { type, values, errors, touched, setFieldValue, accessor, Header, label, className, style, onChange: myOnChange, initialValues } = props;
    const errorMessage = getNestedValue(accessor, errors);
    const nestedTouched = getNestedValue(accessor, touched);
    const defaultValue = undefined;
    const initialValue = (_a = getNestedValue(accessor, initialValues)) !== null && _a !== void 0 ? _a : defaultValue;
    const onChangeHandler = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChange(myOnChangeE);
        }
        else {
            setFieldValue(e.value);
        }
    };
    return _jsxs(FormGroup, { children: [label !== false && _jsx(Form.Label, { children: label !== null && label !== void 0 ? label : Header }, void 0), _jsx(PatternFormat, { name: accessor, valueIsNumericString: true, value: getNestedValue(accessor, values), onValueChange: onChangeHandler, format: "+## ##########", mask: "_", style: style, className: `form-control ${(className !== undefined ? className : '')}`, allowEmptyFormatting: true }, void 0), nestedTouched && _jsx("div", Object.assign({ className: "d-block" }, { children: errorMessage }), void 0)] }, void 0);
}
