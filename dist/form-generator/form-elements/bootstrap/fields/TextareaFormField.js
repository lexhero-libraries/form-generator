import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import { Form } from "react-bootstrap";
import { getNestedValue } from "../../utils/form-generator-utils";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";
export default function TextareaFormField(props) {
    var _a, _b;
    const { disable, values, errors, touched, setFieldValue, accessor, Header, placeholder, label, size, className, style, onChange: myOnChange, initialValues } = props;
    const nestedError = getNestedValue(accessor, errors);
    const nestedTouched = getNestedValue(accessor, touched);
    const defaultValue = "";
    const initialValue = (_a = getNestedValue(accessor, initialValues)) !== null && _a !== void 0 ? _a : defaultValue;
    const onChangeHandler = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.target.value, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChange(myOnChangeE);
        }
        else {
            setFieldValue(e.target.value);
        }
    };
    return _jsxs(Form.Group, Object.assign({ as: "div", style: { position: "relative" } }, { children: [label !== false && _jsx(Form.Label, { children: label !== null && label !== void 0 ? label : Header }, void 0), _jsx(Form.Control, { style: style, className: className, as: "textarea", size: size, isInvalid: nestedError !== undefined, disabled: disable, name: accessor, placeholder: placeholder, value: (_b = getNestedValue(accessor, values)) !== null && _b !== void 0 ? _b : defaultValue, onChange: onChangeHandler }, void 0), _jsx(Form.Control.Feedback, Object.assign({ className: "font-weight-bold", type: "invalid", role: "alert", "aria-label": "from feedback", tooltip: true }, { children: nestedError }), void 0)] }), void 0);
}
