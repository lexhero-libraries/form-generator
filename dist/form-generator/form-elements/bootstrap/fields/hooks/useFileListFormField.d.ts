import 'react-dropzone-uploader/dist/styles.css';
import { FileListElementInterface } from "../../../interfaces/FileListElementInterface";
export default function useFileListFormField(props: FileListElementInterface): {
    existingFiles: any;
    removeFile: (index: number) => void;
    setFieldValue: import("../../../../events/OnChangeEventHandler").SetFieldValue;
    Label: false | JSX.Element;
    FileList: any;
    DropFileZone: false | JSX.Element;
};
