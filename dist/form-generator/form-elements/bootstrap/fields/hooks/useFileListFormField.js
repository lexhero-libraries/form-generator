import { jsx as _jsx, jsxs as _jsxs, Fragment as _Fragment } from "react/jsx-runtime";
import { Button, Form } from "react-bootstrap";
import Dropzone from "react-dropzone-uploader";
import DropzonePreview from "../../../utils/DropzonePreview";
import 'react-dropzone-uploader/dist/styles.css';
import { getNestedValue } from "../../../utils/form-generator-utils";
import OnChangeEventHandler from "../../../../events/OnChangeEventHandler";
export default function useFileListFormField(props) {
    var _a, _b, _c;
    const { type, values, disable, errors, touched, setFieldValue, accessor, Header, label, accept, maxFiles = 1, maxFileSizeMB = 10, removeButton, texts, onChange: myOnChange, initialValues, showDropzone = true, showList = true, dropzoneClassNames, dropzoneStyles, dropzoneContent } = props;
    const checkedTexts = {
        remove: (_a = texts === null || texts === void 0 ? void 0 : texts.remove) !== null && _a !== void 0 ? _a : "Rimuovi file",
        upload: (_b = texts === null || texts === void 0 ? void 0 : texts.upload) !== null && _b !== void 0 ? _b : "Carica file",
        error: (_c = texts === null || texts === void 0 ? void 0 : texts.error) !== null && _c !== void 0 ? _c : "File troppo grande"
    };
    function initialValueCheck(params) {
        if (Array.isArray(params))
            return params;
        if (params !== undefined)
            console.error('initialValue non è un array');
        return [];
    }
    const initialValue = initialValueCheck(getNestedValue(accessor, initialValues));
    const existingFiles = getNestedValue(accessor, values) || [];
    const removeFile = (index) => {
        const newFiles = existingFiles.filter((_, i) => i !== index);
        setFieldValue(newFiles);
    };
    const onChangeHandler = (files) => {
        // todo se vuoi il base64 fai diventare la function async e decommenta
        // for (const i in files) {
        //     files[i].base64 = await readFile(files[i].file)
        // 
        const remainingSlots = maxFiles - existingFiles.length;
        const newFiles = files.slice(0, remainingSlots);
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(newFiles, existingFiles, setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue([...existingFiles, ...newFiles]);
            myOnChange(myOnChangeE);
        }
        else {
            setFieldValue([...existingFiles, ...newFiles]);
        }
    };
    const isMaxFilesReached = existingFiles.length >= maxFiles;
    const checkedDisable = disable !== undefined
        ? (disable !== null && disable !== void 0 ? disable : isMaxFilesReached)
        : isMaxFilesReached;
    const Label = label !== false && _jsx(Form.Label, { children: label !== null && label !== void 0 ? label : Header }, void 0);
    const FileList = showList && existingFiles.map((file, index) => (_jsxs("div", { children: [file.file.name && _jsxs("span", { children: [file.file.name, " "] }, void 0), _jsx("span", Object.assign({ style: { cursor: 'pointer' }, onClick: () => removeFile(index) }, { children: removeButton !== null && removeButton !== void 0 ? removeButton : _jsx(Button, { children: checkedTexts.remove }, void 0) }), void 0)] }, index)));
    const DropFileZone = showDropzone && _jsxs(_Fragment, { children: [_jsx(Dropzone, { onSubmit: (successFiles) => {
                    const files = successFiles.map(file => file.file);
                }, onChangeStatus: (file, status, allFiles) => {
                    if (status === "error_file_size") {
                        throw new Error(checkedTexts.error);
                    }
                    if (status === "done") {
                        if (allFiles[allFiles.length - 1] === file) {
                            onChangeHandler([...allFiles]);
                            const filesToRemove = [...allFiles];
                            filesToRemove.forEach((fileToRemove) => {
                                fileToRemove.remove();
                            });
                        }
                    }
                }, submitButtonDisabled: true, submitButtonContent: _jsx("button", { style: { height: "0!important", width: "0!important" }, hidden: true }, void 0), PreviewComponent: DropzonePreview, accept: accept, maxFiles: maxFiles, maxSizeBytes: maxFileSizeMB * 1024 * 1024, inputContent: dropzoneContent !== null && dropzoneContent !== void 0 ? dropzoneContent : checkedTexts.upload, classNames: dropzoneClassNames, styles: dropzoneStyles, disabled: checkedDisable }, void 0), _jsxs("p", Object.assign({ style: { fontSize: 10, opacity: checkedDisable ? 0.5 : 1 } }, { children: ["Max File Size: ", maxFileSizeMB, "MB"] }), void 0)] }, void 0);
    return {
        existingFiles,
        removeFile,
        setFieldValue,
        Label,
        FileList,
        DropFileZone,
    };
}
