import { jsxs as _jsxs } from "react/jsx-runtime";
import useFileListFormField from "./hooks/useFileListFormField";
export default function FileListFormField(props) {
    const { Label, FileList, DropFileZone } = useFileListFormField(props);
    return (_jsxs("div", Object.assign({ style: props === null || props === void 0 ? void 0 : props.style, className: props === null || props === void 0 ? void 0 : props.className }, { children: [Label, FileList, DropFileZone] }), void 0));
}
