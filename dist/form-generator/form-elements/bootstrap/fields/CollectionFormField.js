import { jsx as _jsx, Fragment as _Fragment, jsxs as _jsxs } from "react/jsx-runtime";
import { Button, Col, Row } from "react-bootstrap";
import React, { useContext } from "react";
import FormGeneratorContext from "../../../form-context/FormGeneratorContext";
import FormGeneratorContextProvider from "../../../form-context/FormGeneratorContextProvider";
import { getNestedValue } from "../../utils/form-generator-utils";
import FormDescriptor from "../../../form-descriptor/FormDescriptor";
export default function CollectionFormField({ accessor, nestedForm, rows = 1, addButton: addButtonProps, removeButton: removeButtonProps, initialValues: initialValuesList, lockList = false, hideSeparator = false }) {
    const { setFieldValue, disable, values, errors, elements, formValue, unsetFieldValue } = useContext(FormGeneratorContext);
    const initialValues = (typeof initialValuesList === "object" || initialValuesList === undefined) ? initialValuesList : initialValuesList();
    const existingElements = getNestedValue(accessor, values);
    const showButton = (!disable && !lockList);
    const addButton = showButton && ((addButtonProps) ? React.cloneElement(addButtonProps, { onClick: (e) => { e.preventDefault(); setFieldValue(`${accessor}[${existing}]`, initialValues); } }) : _jsx(Button, Object.assign({ type: "button", onClick: (e) => { e.preventDefault(); setFieldValue(`${accessor}[${existing}]`, initialValues); } }, { children: "+" }), void 0));
    const removeButton = (indexAccessor) => {
        return showButton && ((removeButtonProps) ? React.cloneElement(removeButtonProps, { onClick: () => unsetFieldValue(indexAccessor) }) : _jsx(Button, Object.assign({ onClick: () => unsetFieldValue(indexAccessor) }, { children: "-" }), void 0));
    };
    const collectionElement = elements.find(element => element.accessor === accessor);
    const existing = getNestedValue(accessor, values).length;
    // @ts-ignore
    const nestedElements = collectionElement.formElements;
    const formDescriptor = new FormDescriptor({ elements: nestedElements, initialValues });
    const suddivisioni = suddividiInGruppi(existingElements, rows);
    const nestedForms = suddivisioni.map((gruppi, gruppoIndex) => {
        return _jsxs(_Fragment, { children: [_jsx(Row, Object.assign({ className: "mb-3" }, { children: gruppi.map((element, index) => {
                        const trueIndex = gruppoIndex * rows + index;
                        const indexAccessor = `${accessor}[${trueIndex}]`;
                        return _jsxs(_Fragment, { children: [showButton && _jsx(Col, Object.assign({ xs: 1, className: "d-flex justify-content-center align-items-center" }, { children: removeButton(indexAccessor) }), void 0), _jsx(Col, Object.assign({ xs: getColXS(rows, showButton) }, { children: _jsx(FormGeneratorContextProvider, { existingErrors: getNestedValue(indexAccessor, errors), disable: disable, formValue: formValue, formDescriptor: formDescriptor, existingValue: getNestedValue(indexAccessor, values), accessorRoot: indexAccessor, onChange: (value) => { setFieldValue(indexAccessor, value); }, children: nestedForm ? nestedForm(trueIndex, element) : undefined }, trueIndex) }), void 0)] }, void 0);
                    }) }), gruppoIndex), !hideSeparator && _jsx("hr", {}, void 0)] }, void 0);
    });
    if (collectionElement === undefined)
        return _jsx("div", { children: accessor }, void 0);
    return _jsxs(_Fragment, { children: [nestedForms, addButton] }, void 0);
}
function getColXS(rows, showButton) {
    if (showButton) {
        switch (rows) {
            case 1: return 11;
            case 2: return 5;
            case 3: return 3;
            case 4: return 2;
        }
    }
    else {
        switch (rows) {
            case 1: return 12;
            case 2: return 6;
            case 3: return 4;
            case 4: return 3;
        }
    }
}
function suddividiInGruppi(array, dimensioneGruppo) {
    return array.reduce((acc, _, i) => {
        if (i % dimensioneGruppo === 0) {
            acc.push(array.slice(i, i + dimensioneGruppo));
        }
        return acc;
    }, []);
}
