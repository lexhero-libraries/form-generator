import { jsx as _jsx, Fragment as _Fragment, jsxs as _jsxs } from "react/jsx-runtime";
import { Button, Form } from "react-bootstrap";
import React from "react";
import Dropzone from "react-dropzone-uploader";
import DropzonePreview from "../../utils/DropzonePreview";
import { readFile } from "../../utils/FileUploadedHelper";
import 'react-dropzone-uploader/dist/styles.css';
import { getNestedValue } from "../../utils/form-generator-utils";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";
export default function FileFormField(props) {
    var _a;
    const { type, values, disable, errors, touched, setFieldValue, accessor, Header, label, accept, maxFileSizeMB = 10, removeButton: removeFileButtonProps, texts = {
        remove: "Rimuovi file",
        download: "Scarica file",
        upload: "Carica file",
        error: "File troppo grande",
        noContent: "Nessun file caricato"
    }, onChange: myOnChange, initialValues } = props;
    const defaultValue = undefined;
    const initialValue = (_a = getNestedValue(accessor, initialValues)) !== null && _a !== void 0 ? _a : defaultValue;
    const existingFile = getNestedValue(accessor, values);
    const onDownloadFile = () => { window.open(process.env.REACT_APP_ENTRYPOINT + existingFile.url); };
    const removeFileButton = ((removeFileButtonProps) ? React.cloneElement(removeFileButtonProps, { onClick: (e) => { e.preventDefault(); setFieldValue(null); } }) : _jsx(Button, Object.assign({ onClick: () => { setFieldValue(null); } }, { children: texts.remove }), void 0));
    const onChangeHandler = (file, result) => {
        if (myOnChange) {
            const value = { file, fileParsed: result };
            const myOnChangeE = new OnChangeEventHandler(value, existingFile, setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(result);
            myOnChangeE.reset = () => {
                file.remove();
                return setFieldValue(initialValue);
            };
            myOnChange(myOnChangeE);
        }
        else {
            setFieldValue(result);
        }
    };
    return _jsxs(_Fragment, { children: [label !== false && _jsx(Form.Label, { children: label !== null && label !== void 0 ? label : Header }, void 0), existingFile && _jsxs(_Fragment, { children: [existingFile.url && _jsx(Button, Object.assign({ onClick: onDownloadFile }, { children: texts.download }), void 0), !disable && removeFileButton] }, void 0), !existingFile && !disable && _jsxs(_Fragment, { children: [_jsx(Dropzone, { onSubmit: (successFiles) => {
                            const files = successFiles.map(file => file.file);
                        }, onChangeStatus: (file, status, allFiles) => {
                            if (status === "error_file_size") {
                                throw new Error(texts.error);
                            }
                            if (status === "done") {
                                readFile(file.file).then(result => { onChangeHandler(file, result); });
                            }
                        }, submitButtonDisabled: true, submitButtonContent: _jsx("button", { style: { height: "0!important", width: "0!important" }, hidden: true }, void 0), PreviewComponent: DropzonePreview, accept: accept, maxFiles: 1, maxSizeBytes: maxFileSizeMB * 1024 * 1024, inputContent: texts.upload }, void 0), _jsxs("p", Object.assign({ style: { fontSize: 10 } }, { children: ["Max File Size: ", maxFileSizeMB, "MB"] }), void 0)] }, void 0), !existingFile && disable && _jsx("div", { children: texts.noContent }, void 0)] }, void 0);
}
