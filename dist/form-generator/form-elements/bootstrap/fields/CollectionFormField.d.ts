import { CollectionElementInterface } from "../../interfaces/CollectionElementInterface";
export default function CollectionFormField({ accessor, nestedForm, rows, addButton: addButtonProps, removeButton: removeButtonProps, initialValues: initialValuesList, lockList, hideSeparator }: CollectionElementInterface): JSX.Element;
