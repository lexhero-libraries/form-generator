import { jsx as _jsx } from "react/jsx-runtime";
import { Form } from "react-bootstrap";
import { getNestedValue } from "../../utils/form-generator-utils";
import { FormGroup } from "../../utils/FormGroup";
import OnChangeEventHandler from "../../../events/OnChangeEventHandler";
export default function CheckboxFormField(props) {
    var _a, _b;
    const { type, values, errors, touched, setFieldValue, accessor, Header, className, style, onChange: myOnChange, initialValues } = props;
    const nestedError = getNestedValue(accessor, errors);
    const nestedTouched = getNestedValue(accessor, touched);
    const defaultValue = false;
    const initialValue = (_a = getNestedValue(accessor, initialValues)) !== null && _a !== void 0 ? _a : defaultValue;
    const onChangeHandler = (e) => {
        if (myOnChange) {
            const myOnChangeE = new OnChangeEventHandler(e.target.checked, getNestedValue(accessor, values), setFieldValue, initialValue);
            myOnChangeE.defaultSetter = () => setFieldValue(!getNestedValue(accessor, values));
            myOnChange(myOnChangeE);
        }
        else {
            setFieldValue(!getNestedValue(accessor, values));
        }
    };
    return _jsx(FormGroup, { children: _jsx(Form.Check, { style: style, className: className, name: accessor, type: "checkbox", label: Header, id: accessor, onChange: onChangeHandler, checked: (_b = getNestedValue(accessor, values)) !== null && _b !== void 0 ? _b : defaultValue, isInvalid: nestedError !== undefined, feedback: nestedError, feedbackType: "invalid", feedbackTooltip: true }, void 0) }, void 0);
}
