import { CSSProperties } from "react";
import { SelectOption } from "./interfaces/SelectElementInterface";
import { RadioOption } from "./interfaces/RadioElementInterface";
import OnChangeEventHandler from "../events/OnChangeEventHandler";
interface FormElementInterface {
    accessor: string;
    onChange?: (OnChangeEvent: OnChangeEventHandler) => void;
    className?: string;
    style?: CSSProperties;
    nestedForm?: (index: number, element: any) => JSX.Element;
    placeholder?: string;
    label?: string | false;
    options?: SelectOption[] | RadioOption[];
    addButton?: any;
    removeButton?: any;
    disable?: boolean;
    size?: "sm" | "lg";
    texts?: {
        remove: string;
        download: string;
        upload: string;
        error: string;
        noContent: string;
    };
}
export default function FormElement({ accessor, nestedForm, options, ...others }: FormElementInterface): JSX.Element;
export declare function useFormElementValue(accessor: string): any;
export {};
