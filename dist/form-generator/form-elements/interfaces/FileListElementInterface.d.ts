import { IStyleCustomization } from "react-dropzone-uploader";
import BasicFormElementInterface from "../../BasicFormElementInterface";
import { FileType } from "../../ElementInterface";
import { CSSProperties } from "react";
export interface FileListElementInterface extends BasicFormElementInterface {
    type: "fileList";
    accept: FileType;
    maxFiles?: number;
    maxFileSizeMB?: number;
    removeButton?: JSX.Element;
    dropzoneContent?: JSX.Element;
    showDropzone?: boolean;
    showList?: boolean;
    dropzoneClassNames?: IStyleCustomization<string>;
    dropzoneStyles?: IStyleCustomization<CSSProperties>;
    texts?: {
        remove?: string;
        upload?: string;
        error?: string;
    };
}
