import { WithButtonElementInterface } from "../../BasicFormElementInterface";
import { FormElements } from "../../ElementInterface";
export declare type Rows = 1 | 2 | 3 | 4;
export interface CollectionElementInterface extends WithButtonElementInterface {
    type: "collection";
    formElements: FormElements;
    nestedForm: (index: number, element: any) => JSX.Element;
    initialValues: (object | (() => object));
    lockList?: boolean;
    rows?: Rows;
    hideSeparator?: boolean;
}
