var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import { jsx as _jsx } from "react/jsx-runtime";
import { useContext } from "react";
import FormGeneratorContext from "../form-context/FormGeneratorContext";
import { getAccessorElementsNoIndex, getNestedValue } from "./utils/form-generator-utils";
import FormElementGenerator from "./FormElementGenerator";
function getElement(elements, accessorParsed) {
    if (accessorParsed.length <= 0)
        throw new Error("Accessore non valido.");
    const piece = elements.find(formElement => formElement.accessor === accessorParsed[0]);
    if (piece === undefined)
        throw new Error(`Elemento ${accessorParsed[0]} non trovato nel formDescriptor.`);
    if (accessorParsed.length === 1)
        return piece;
    if ("formElements" in piece)
        return getElement(piece.formElements, accessorParsed.slice(1));
    throw new Error("Errore di configurazione del formElements annidato.");
}
export default function FormElement(_a) {
    var { accessor, nestedForm, options } = _a, others = __rest(_a, ["accessor", "nestedForm", "options"]);
    const { values, errors, touched, setFieldValue, elements, formValue, initialValues } = useContext(FormGeneratorContext);
    const accessorParsed = getAccessorElementsNoIndex(accessor);
    const element = getElement(elements, accessorParsed);
    const finalOptions = getElementOptions(options, element);
    if (element) {
        // @ts-ignore
        return _jsx(FormElementGenerator, Object.assign({ nestedForm: nestedForm }, element, others, { values: values, initialValues: initialValues, errors: errors, touched: touched, setFieldValue: (value) => setFieldValue(accessor, value), accessor: accessor, options: finalOptions }), void 0);
    }
    return _jsx("div", { children: accessor }, void 0);
}
export function useFormElementValue(accessor) {
    const { values, errors, touched, setFieldValue, elements, disable } = useContext(FormGeneratorContext);
    return getNestedValue(accessor, values);
}
function getElementOptions(options, element) {
    if (options)
        return options;
    if ("options" in element)
        return element.options;
    return undefined;
}
