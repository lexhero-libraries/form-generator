export default class OnChangeEventHandler {
    constructor(value, accessorCurrentValue, setFieldValue, initialValue) {
        this.defaultSetter = () => this.setFieldValue(this.value);
        this.reset = () => this.setFieldValue(this.initialValue);
        this.value = value;
        this.accessorCurrentValue = accessorCurrentValue;
        this.setFieldValue = setFieldValue;
        this.initialValue = initialValue;
    }
}
