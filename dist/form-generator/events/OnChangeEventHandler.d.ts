import { FormikErrors, FormikValues } from "formik";
export declare type SetFieldValue = (value: any) => Promise<void> | Promise<FormikErrors<FormikValues>>;
export default class OnChangeEventHandler {
    value: any;
    accessorCurrentValue: any;
    setFieldValue: SetFieldValue;
    initialValue: any;
    constructor(value: any, accessorCurrentValue: any, setFieldValue: SetFieldValue, initialValue: any);
    defaultSetter: () => Promise<void> | Promise<FormikErrors<FormikValues>>;
    reset: () => Promise<void> | Promise<FormikErrors<FormikValues>>;
}
