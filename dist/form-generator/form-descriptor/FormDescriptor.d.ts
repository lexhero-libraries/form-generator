import { FormElements } from "../ElementInterface";
import * as Yup from "yup";
import { FormDescriptorAddElementInterface } from "./FormDescriptorAddElementInterface";
interface FormDescriptorInterface {
    elements: FormElements;
    validationSchema?: Yup.ObjectSchema<any>;
    initialValues: ((() => object) | object);
    resetOnSubmit?: boolean;
}
export default class FormDescriptor {
    elements: FormElements;
    validationSchema: Yup.ObjectSchema<any>;
    initialValues: ((() => object) | object);
    resetOnSubmit: boolean;
    constructor({ elements, validationSchema, initialValues, resetOnSubmit }: FormDescriptorInterface);
    addElement(element: FormDescriptorAddElementInterface, initialValue: any, validationRule?: any): void;
}
export {};
