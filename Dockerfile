FROM node:18-alpine
USER root
RUN apk update && apk add bash

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh shadow yarn

#RUN adduser -u 501 -D $USER -g staff
WORKDIR /app
#RUN chown -R $USER:$USER .

USER node

COPY package*.json /app
COPY yarn.lock /app

# Run the app when the container launches
CMD yarn install; yarn start
